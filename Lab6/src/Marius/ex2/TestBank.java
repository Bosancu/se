package Marius;

public class TestBank {
	public static void main(String[] args) {
		Bank banca = new Bank();

		banca.addAccount("alo", 1000);
		banca.addAccount("salut", 2000);
		banca.addAccount("sunt", 100);
		banca.addAccount("eu", 2300);
		banca.addAccount("unhaiduc", 4000);

		banca.printAccounts(500, 2000);
	}
}
