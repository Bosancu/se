package Marius;

import Marius.BankAccount;
import java.util.*;

public class Comp implements Comparator<BankAccount> {
	public int compare(BankAccount ac1, BankAccount ac2) {
		return ((int)ac1.getBalance() - (int)ac2.getBalance());
	}
}
