package Marius;

import Marius.BankAccount;
import java.util.*;

public class Bank {
	private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

	public void addAccount(String owner, double balance) {
		accounts.add(new BankAccount(owner, balance));
	}
	public void printAccounts() {
		Collections.sort(accounts, new Comp());
		for (BankAccount i : accounts) {
			System.out.println(i.toString());
		}
	}

	public void printAccounts(double minBalance, double maxBalance) {
		//sort the list by balance
		Collections.sort(accounts, new Comp());
		for (BankAccount i : accounts) {
			if (i.getBalance() >= minBalance && i.getBalance() <= maxBalance)
				System.out.println(i.toString());
		}
	}
	
	public BankAccount getAccount(String owner) {
		for (BankAccount i : accounts) {
			if (i.getOwner() == owner)
				return (i);
		}
		return (null);
	}
}
