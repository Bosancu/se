package Marius;

import java.util.HashMap;

public class Dictionary {
	HashMap<Word, Definition> dictionary = new HashMap<Word, Definition>();

	public Dictionary() {}
	
	public void			addWord(Word w, Definition d) {
		dictionary.put(w, d);
	}
	public Definition	getDefinition(Word w) {
		return (dictionary.get(w));
	}
	public void			getAllWords() {
		for (Word key : dictionary.keySet()) {
			System.out.println(key.getWord());
		}
	}
	public void			getAllDefinitions() {
		for (Word key : dictionary.keySet()) {
			System.out.println(dictionary.get(key).getDefinition());
		}
	}
	public void			printDictionary() {
		for (Word key : dictionary.keySet()) {
			System.out.println(key.getWord() + " - " + dictionary.get(key).getDefinition());
		}
	}
	public HashMap<Word, Definition> getHashMap() {
		return (this.dictionary);
	}
}
