package Marius;

import Marius.BankAccount;
import Marius.Comp;
import java.util.*;

public class Bank {
	private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(new Comp());

	public void addAccount(String owner, double balance) {
		accounts.add(new BankAccount(owner, balance));
	}
	public void printAccounts() {
		for (BankAccount i : accounts) {
			System.out.println(i.toString());
		}
	}

	public void printAccounts(double minBalance, double maxBalance) {
		//sort the list by balance
		for (BankAccount i : accounts) {
			if (i.getBalance() >= minBalance && i.getBalance() <= maxBalance)
				System.out.println(i.toString());
		}
	}
	
	public BankAccount getAccount(String owner) {
		for (BankAccount i : accounts) {
			if (i.getOwner() == owner)
				return (i);
		}
		return (null);
	}
}
