package Marius;

public class TestBank {
	public static void main(String[] args) {
		BankAccount x, y, z;
		x = new BankAccount("bau");
		y = new BankAccount("buahaha");
		z = new BankAccount("mere", 1234);

		System.out.println(x.equals(y) + " " + x.equals(z));
		System.out.println(z.hashCode());
	}
}
