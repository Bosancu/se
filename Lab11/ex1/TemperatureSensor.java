package Marius;

import java.util.ArrayList;
import java.util.List;

public class TemperatureSensor extends Thread {
    private Monitor monitor;
    private double temperature;

    TemperatureSensor(Monitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public void run() {
        while (true) { 
            temperature = Math.random() * 100;
            monitor.update(temperature);
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println("exception in thread sleep");
            }
        }
    }
}
