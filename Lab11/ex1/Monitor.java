package Marius

public class Monitor {

    public void update(double value) {
        System.out.println("Change in temperature; temperature in now: "
                + value);
    }
}
