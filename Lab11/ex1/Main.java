package Marius;

public class Main {

    public static void main(String[] args) {
        Monitor monitor = new Monitor();
        TemperatureSensor temp = new TemperatureSensor(monitor);

        temp.start();

    }
}
