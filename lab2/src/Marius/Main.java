package Marius;
import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

		//ex1
		Scanner scan = new Scanner(System.in);
		System.out.print("EX1 : enter a number: ");
		int a = scan.nextInt();
		System.out.print("enter another one: ");
		int b = scan.nextInt();
		System.out.println(Math.max(a, b));
		//ex2
		System.out.println("EX2 : a = ");
		if (a == 1)
			System.out.println("one");
		else if (a == 2)
			System.out.println("two");
		else if (a == 3)
			System.out.println("three");
		else if (a == 4)
			System.out.println("four");
		else if (a == 5)
			System.out.println("five");
		else if (a == 6)
			System.out.println("six");
		else if (a == 7)
			System.out.println("seven");
		else if (a == 8)
			System.out.println("eight");
		else if (a == 9)
			System.out.println("nine");
		else
			System.out.println("other");
		System.out.println("b = ");
		switch (b) {
			case 1:
				System.out.println("one");
				break;
			case 2:
				System.out.println("two");
				break;
			case 3:
				System.out.println("three");
				break;
			case 4:
				System.out.println("four");
				break;
			case 5:
				System.out.println("five");
				break;
			case 6:
				System.out.println("six");
				break;
			case 7:
				System.out.println("seven");
				break;
			case 8:
				System.out.println("eight");
				break;
			case 9:
				System.out.println("nine");
				break;
			default:
				System.out.println("other");
		}


		//EX3
		System.out.println("EX3 = ");
		int n;
		int count = 0;
		for (n = a; n <= b; n++) {
			if (!prime(n)) {
				System.out.print(n + " ");
				count++;
			}
		}
		System.out.println("\n" + count + " nr.prime");

		//EX4
		System.out.print("give length of array");
		int k = scan.nextInt();
		int[] sir = new int[k];
		int i;
		for (i = 0; i < k; i++) {
			System.out.println("give " + i + " number: ");
			sir[i] = scan.nextInt();
		}
		int max = sir[0];

		for (i = 1; i < k; i++) {
			if (sir[i] > max) {
				max = sir[i];
			}
		}
		System.out.println("max nr is : " + max);

		//EX5
		Random rand = new Random();
		int[] arr = new int[10];
		System.out.println("array intial");
		for (i = 0; i < 10; i++) {
			arr[i] = rand.nextInt(100);
			System.out.print(" " + arr[i]);
		}
		bubbleSort(arr, 10);
		System.out.println("\n");
		for (i = 0; i < 10; i++) {
			System.out.print(" " + arr[i]);
		}

		//EX6
		System.out.print("give a number: ");
		int w = scan.nextInt();
		long fact = 1;

		for (i = 1; i <= w; i++) {
			fact *= i;
		}
		System.out.println(fact);

		//EX7
		Random rando = new Random();
		int l = rando.nextInt(100) + 1;
		System.out.print("enter a number: ");
		int e = 0;
		for (i = 0; i < 3; i++) {
			e = scan.nextInt();
			if (e == l) {
				System.out.println("such cool , much wow");
				break ;
			}
			else if (e < l)
				System.out.println("too low");
			else
				System.out.println("too high");
		}
		if (e != l) {
			System.out.println("You lost");
			System.out.println("correct number was: " + l);
		}
		scan.close();
	}
	static void bubbleSort(int arr[], int n) {
		int i, j, temp;
		boolean swapped;
		for (i = 0; i < n - 1; i++) {
			swapped = false;
			for (j = 0; j < n - i - 1; j++) {
				if (arr[j] > arr[j + 1]) {
					// swap arr[j] and arr[j+1]
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
					swapped = true;
				}
			}
		}
	}
		public static boolean prime(int n){
		boolean what = false;
		int i;
		for (i = 2; i <= n / 2; i++) {
			if (n % i == 0) {
				what = true;
				break ;
			}
		}
		return (what);
	}


}


