package Marius;

public class Robot {
    private int x;

    public Robot() {
        this.x = 1;
    }
    public void change(int k) {
        if (k > 0){
            this.x = this.x + k ;
        }
    }
    public String toString() {
        return ("position of robot is: " + this.x); }
}

