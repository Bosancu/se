package Marius;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint a = new MyPoint();
        MyPoint b = new MyPoint(3, 5);

        System.out.println("distance from a to b: " + a.distance(b));
    }
}
