package Marius;

public class Circle {
    private double r = 1;
    private String color = "red";

    public Circle() {
        this.r = r;
        this.color = color;
    }
    public Circle(double rad, String col) {
        this.r = rad;
        this.color = col;
    }
    public double getRadius() { return (this.r); }
    public double getArea() { return (Math.PI * this.r * this.r); }
}
