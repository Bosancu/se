package Marius;

public class Flower {
    private int petal;
    static int flowers = 0;

    Flower(int p) {
        flowers++;
        petal = p;
        System.out.println("New flower has been created!");
    }
}