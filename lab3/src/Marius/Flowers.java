package Marius;

public class Flowers{
    public static void main(String[] args) {
        Flower f1 = new Flower(40);
        Flower f2 = new Flower(32);
        Flower f3 = new Flower(22);
        Flower f4 = new Flower(10);
        Flower f5 = new Flower(2);
        Flower f6 = new Flower(1);
        System.out.println("we have " + f1.flowers + " flowers");
    }
}
