package Marius;

public class TestCircle {
    public static void main(String[] args) {
        Circle cerc = new Circle();
        System.out.println(cerc.getRadius() + " " + cerc.getArea());
        cerc = new Circle(3, "blue");
        System.out.println(cerc.getRadius() + " " + cerc.getArea());
    }
}