package Marius;

public class RectangleEx6 extends Shape {
    private double width = 1.0;
    private double length = 1.0;

    public RectangleEx6() { super(); }
    public RectangleEx6(double width, double length) {
        super();
        this.width = width;
        this.length = length;
    }
    public RectangleEx6(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double	getWidth() { return (this.width); }
    public void		setWidth(double width) { this.width = width; }
    public void		setLength(double length) { this.length = length; }
    public double	getLength() { return (this.length); }
    public double	getArea() { return (this.width * this.length); }
    public double	getPerimeter() { return (2 * (this.width + this.length)); }

    public String	toString() {
        return ("A rectangle with width=" + this.width + " and length=" + this.length + "which is a subclass of " + super.toString());
    }
}
