package Marius;

import Marius.Author;

public class 	Book {
    private String name;
    private Author author;
    private double price;
    private int Stock;

    public 		Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public 		Book(String name, Author author, double price, int Stock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.Stock = Stock;
    }

    public String 	getName() { return (this.name); }
    public Author 	getAuthor() { return (this.author); }
    public double 	getPrice() { return (this.price); }
    public void		setPrice(double price) { this.price = price; }
    public int		Stock() { return (this.Stock); }
    public void		Stock(int Stock) { this.Stock = Stock; }
    public String toString() {
        return (this.name + " by " + author.toString());
    }
}