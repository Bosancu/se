package Marius;

public class CircleEx6 extends Shape {
    private double radius = 1.0;

    public CircleEx6() { super(); }
    public CircleEx6(double radius) {
        super();
        this.radius = radius;
    }
    public CircleEx6(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double	getRadius() { return (this.radius); }
    public void 		setRadius(double radius) {
        this.radius = radius;
    }
    public double	getArea() {
        return (Math.PI * this.radius * this.radius);
    }
    public double	getPerimeter() {
        return (2 * Math.PI * this.radius);
    }
    public String	toString() {
        return ("A Circle with radius=" + this.radius + ", which is a subclass of " + super.toString());
    }
}
