package Marius;

import Marius.Author;

public class 	BookEx4 {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public 		BookEx4(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }
    public 		BookEx4(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String 	getName() { return (this.name); }
    public Author[]	getAuthors() { return (this.authors); }
    public double 	getPrice() { return (this.price); }
    public void		setPrice(double price) { this.price = price; }
    public int		getQtyInStock() { return (this.qtyInStock); }
    public void		setQtyInStock(int qtyInStock) { this.qtyInStock = qtyInStock; }
    public String	toString() {
        return (this.name + " by " + this.authors.length + " authors");
    }
    public void		printAuthors() {
        for (Author a: this.authors) {
            System.out.println(a.toString());
        }
    }
}
