package Marius;

import javax.swing.JFrame;
 
public class SimpleApp extends JFrame{
 
      SimpleApp(){
            setTitle("this is some app");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(400,500);
            setVisible(true);
            setResizable(false);
      }
 
      public static void main(String[] args) {
            SimpleApp a = new SimpleApp();
      }
 
}
