package Marius;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class Index extends JFrame {

    Index() {
        setTitle("simple index app");
        setSize(400, 400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
        JTextField text = new JTextField();
        add(text);

        int i = 0;
        JButton button = new JButton("inc button");
        add(button);

        button.addActionListener(new ActionListener() {
            private int i;
            {
                updateText();
            }

            public void actionPerformed(ActionEvent e) {
                ++i;
                updateText();
            }

            private void updateText() {
                button.setText(Integer.toString(i));
            }
        });
    }

    public static void main(String[] args) {
        Index a = new Index();
    }
}
