package Marius;

import java.io.BufferedReader;
import java.lang.*;
import java.io.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class App extends JFrame {

    App() {
        setTitle("simple index app");
        setSize(400, 400);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
        JTextField text = new JTextField();
        add(text);
        text.setLocation(10, 20);
        text.setSize(100, 20);
        text.setEditable(true);
        text.setText("enter file name");

        JTextField content = new JTextField();
        add(content);
        content.setBounds(20, 200, 300, 100);

        JButton button = new JButton("read file");
        add(button);
        button.setBounds(10, 100, 200, 50);

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String fileName = text.getText();
                content.setText("");
                try (BufferedReader file = new BufferedReader(new FileReader(fileName))) {
                    String fileContent = "";
                    while ((fileContent = file.readLine()) != null) {
                        content.setText(content.getText() + "\n" + fileContent);
                    }
                }
                catch(IOException ioe) {
                    System.out.println("file not found " + fileName);
                }
            }
        });
    }

    public static void main(String[] args) {
        App a = new App();
    }
}
