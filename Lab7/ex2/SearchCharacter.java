package Marius;

import java.util.*;
import java.io.*;

public class SearchCharacter {
	public static void main(String[] args) {
		char c;
		Scanner scanner = new Scanner(System.in);

		System.out.println("enter chracter to be searched in data.txt: ");
		c = scanner.next().charAt(0);
		scanner.close();
		int k = 0;
		
		try {
			FileReader fd = new FileReader("data.txt");
			int i;

			while ((i = fd.read()) != -1)
				k+= ((char)i == c ? 1 : 0);
		} catch (FileNotFoundException e) {}
		catch (IOException e) {}

		System.out.println("in data.txt we have " + k + " " + c);
	}
}
