package Marius;

import java.io.*;
import java.util.*;

public class Dealer {
	public Dealer() {}

	public void addCar(String model, double price) throws IOException {
		Car car = new Car(model, price);

		ObjectOutputStream o =
			new ObjectOutputStream(
				new FileOutputStream("MB/cars" + model));

		o.writeObject(car);
	}

	public void getCar(String model) throws IOException, ClassNotFoundException {
		ObjectInputStream in =
			new ObjectInputStream(
				new FileInputStream("MB/cars" + model));

		Car car = (Car)in.readObject();
		System.out.println(car);
	}
}
