package Marius;

import java.util.*;

public class TestDealer {
	public static void main(String[] args) throws Exception {
		Dealer dealer = new Dealer();
		Scanner in = new Scanner(System.in);
		String model;
		double price;
		int opt;

		while (true) {
			System.out.println("1. add car\n2. see car details\n");
			opt = in.nextInt();
			switch (opt) {
				case 1: {
					System.out.println("give the car model: ");
					in.nextLine();
					model = in.nextLine();
					System.out.println("give price: ");
					price = in.nextDouble();
					dealer.addCar(model, price);
					break ; }
				case 2: {
					System.out.println("give the model you are looking for: ");
					in.nextLine();
					model = in.nextLine();
					dealer.getCar(model);
					break ;}
				default:
					break ;
			}
		}
	}
}
