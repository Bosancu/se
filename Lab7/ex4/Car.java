package Marius;

import java.util.*;
import java.io.*;

public class Car implements Serializable {
	private String model;
	private double price;

	public Car(String model, double price) {
		this.model = model;
		this.price = price;
	}

	public String toString() {
		return ("we have a " + this.model + " at " + this.price);
	}
}
